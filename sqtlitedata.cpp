#include "sqtlitedata.h"

#include <QDebug>
#include <QFile>

const QString SQLSCRIPT = "://resources/EnergyPlus.sql";

SQtLiteData::SQtLiteData(QString filename, QObject *parent) :
    QObject(parent),
    m_FileName(filename),
    m_Saved(false)
{
    startup("/home/r2d2/CODE/BUILDlab/sqtlite/resources/idd-schema-data.sqlt");
    Schema = new SQtLiteObject("root",
                               qobject_cast<SQtLiteObject *>(this),
                               SQtLiteObject::Root_Schema,
                               SQtLiteObject::Depth_Root,
                               getTableModel("schema_objects"),
                               getTableModel("schema_fields"));
    Model = new SQtLiteObject("root",
                              qobject_cast<SQtLiteObject *>(this),
                              SQtLiteObject::Root_Model,
                              SQtLiteObject::Depth_Root,
                              getTableModel("objects"),
                              getTableModel("fields"));

}

SQtLiteData::~SQtLiteData()
{
    MemDB.close();
    delete Schema;
    delete Model;
}

SQtLiteObject *SQtLiteData::getNewObject(QString object_type,
                                         SQtLiteObject *parent,
                                         SQtLiteObject::RootType root_type,
                                         SQtLiteObject::DepthType depth_type)
{
    switch (root_type) {
    case SQtLiteObject::Root_Schema :
        return new SQtLiteObject(QString(object_type),
                                 parent, root_type,
                                 depth_type,
                                 getTableModel("schema_objects"),
                                 getTableModel("schema_fields"));
        break;
    case SQtLiteObject::Root_Model :
        return new SQtLiteObject(QString(object_type),
                                 parent,
                                 root_type,
                                 depth_type,
                                 getTableModel("objects"),
                                 getTableModel("fields"));
        break;
    default:
        break;
    }

}

QString SQtLiteData::FileName() const
{
    return m_FileName;
}

bool SQtLiteData::Saved() const
{
    return m_Saved;
}

QStringList SQtLiteData::getTablenames()
{
    QStringList tablelist = MemDB.tables(QSql::Tables);
    tablelist.append(MemDB.tables(QSql::Views));
    return tablelist;
}

void SQtLiteData::setFileName(QString arg)
{
    if (m_FileName != arg) {
        m_FileName = arg;
        emit FileNameChanged(arg);
    }
}

void SQtLiteData::setSaved(bool arg)
{
    if (m_Saved != arg) {
        m_Saved = arg;
        emit SavedChanged(arg);
    }
}

void SQtLiteData::startup(QString filename)
{
    DiskDB = QSqlDatabase::addDatabase("QSQLITE","DISK");
    DiskDB.setDatabaseName(filename);
    if (DiskDB.open()) {
        ////////////////
        MemDB = QSqlDatabase::addDatabase("QSQLITE","MEM");
        MemDB.setDatabaseName(":memory:");
        if (MemDB.open()) {


            QSqlQuery attach(MemDB);
            QString attachstr = QString("ATTACH DATABASE '%1' AS %2")
                    .arg(filename)
                    .arg(DiskDB.connectionName());
            if (attach.exec(attachstr)) {
                QSqlQuery insertquery(MemDB);
                QStringList tablesviews = DiskDB.tables(QSql::Tables);
                foreach (QString tableorview, tablesviews) {
                    QString insertstr =
                            QString("INSERT OR REPLACE INTO '%1' SELECT * FROM %2.%1")
                            .arg(tableorview)
                            .arg(DiskDB.connectionName());
                    if (!insertquery.exec(insertstr)){
                        qDebug() << "ERROR: startup INSERT failed - "
                                 << insertquery.lastError();
                        qDebug() << insertstr;
                    }
                }
                QSqlQuery detach(MemDB);
                QString detachstr = QString("DETACH %1").arg(DiskDB.connectionName());
                detach.exec(detachstr);
            } else {
                qDebug() << "ERROR: startup database ATTACH failed- "
                         << attach.lastError();
            }



        }  else {
            QString error_string =
                    QString("ERROR: database :memory: at MEM failed to open - ")
                    .arg(MemDB.lastError().text());
            qDebug() << error_string;
            emit sendMessage(error_string);
        }
        DiskDB.close();
    }  else {
        qDebug() << "ERROR: startup database filename at connection failed to open - "
                 << DiskDB.lastError();
    }
    emit dbLoaded();
}

void SQtLiteData::createNewDB()
{
    //    if (filename == "default") {

    //        //FIXME: extract file to disk, then open

    //        openDiskDB("/home/r2d2/CODE/BUILDlab/sqtlite/resources/idd-schema-data.sqlt");

    //        //        openDiskDB("://resources/idd-schema-data.sqlt");
    //    } else {
    MemDB = QSqlDatabase::addDatabase("QSQLITE","MEM");
    MemDB.setDatabaseName(":memory:");
    if (MemDB.open()) {
        QFile script(SQLSCRIPT);
        if (script.open(QFile::ReadOnly | QFile::Text))
        {
            QString scriptstring(script.readAll());
            QStringList queries = scriptstring.split(QChar(';'));
            QSqlQuery query(MemDB);
            foreach(QString queryString, queries){
                queryString.remove(QRegExp("^\n$")); //this is the last line of the stringlist, that editors automatically insert to ensure success elsewhere
                if (!queryString.isEmpty()) {
                    if (!query.exec(queryString)){
                        QString error_string =
                                QString("ERROR: query failed - %1")
                                .arg(query.lastError().text());
                        qDebug() << error_string;
                        emit sendMessage(error_string);
                    }
                }
            }
            //            qDebug() << MemDB.tables(QSql::AllTables);
            script.close();
        } else {
            QString error_string =
                    QString("ERROR: %1 failed to open - %2")
                    .arg(script.fileName())
                    .arg(script.errorString());
            qDebug() << error_string;
            emit sendMessage(error_string);
        }
    }  else {
        QString error_string =
                QString("ERROR: database :memory: at MEM failed to open - ")
                .arg(MemDB.lastError().text());
        qDebug() << error_string;
        emit sendMessage(error_string);
    }
    //    }
}

void SQtLiteData::saveToDisk()
{ //FIXME: file save to disk... needs factoring
    Q_ASSERT(0 != FileName());
    emit FileNameChanged(FileName());
    DiskDB = QSqlDatabase::addDatabase("QSQLITE","DISK");
    DiskDB.setDatabaseName(FileName());
    if (DiskDB.open()) {
        QFile script(SQLSCRIPT);
        if (script.open(QFile::ReadOnly | QFile::Text))
        {
            QString scriptstring(script.readAll());
            QStringList queries = scriptstring.split(QChar(';'));
            QSqlQuery query(DiskDB);
            foreach(QString queryString, queries){
                queryString.remove(QRegExp("^\n$")); //this is the last line of the stringlist, that editors automatically insert to ensure success elsewhere
                if (!queryString.isEmpty()) {
                    if (!query.exec(queryString)){
                        QString error_string =
                                QString("ERROR: query failed - %1")
                                .arg(query.lastError().text());
                        qDebug() << error_string;
                        emit sendMessage(error_string);
                    }
                }
            }
            //            qDebug() << DiskDB.tables(QSql::AllTables);
            script.close();


            /////////////////

            //    DiskDB = QSqlDatabase::addDatabase("QSQLITE","DISK");
            //    DiskDB.setDatabaseName(filename);
            //    if (DiskDB.open()) {
            QSqlQuery attach(MemDB);
            QString attachstr = QString("ATTACH DATABASE '%1' AS %2")
                    .arg(FileName())
                    .arg(DiskDB.connectionName());
            if (!attach.exec(attachstr)) {
                qDebug() << "ERROR: openDiskDB database ATTACH failed- "
                         << attach.lastError();
            }
            QSqlQuery insertquery(MemDB);
            QStringList tablesviews = MemDB.tables(QSql::Tables);
            foreach (QString tableorview, tablesviews) {
                QString insertstr =
                        QString("INSERT OR REPLACE INTO %2.%1 SELECT * FROM '%1'")
                        .arg(tableorview)
                        .arg(DiskDB.connectionName());
                if (!insertquery.exec(insertstr)){
                    qDebug() << "ERROR: openDiskDB INSERT failed - "
                             << insertquery.lastError();
                }
            }
            QSqlQuery detach(MemDB);
            QString detachstr = QString("DETACH %1")
                    .arg(DiskDB.connectionName());
            detach.exec(detachstr);
            DiskDB.close();
            //    }  else {
            //        qDebug() << "ERROR: openDiskDB database filename at connection failed to open - "
            //                 << DiskDB.lastError();
            //    }

            //////////////////////
            emit FileNameChanged(FileName());
        } else {
            QString error_string =
                    QString("ERROR: %1 failed to open - %2")
                    .arg(script.fileName())
                    .arg(script.errorString());
            qDebug() << error_string;
            emit sendMessage(error_string);
        }
    }  else {
        QString error_string =
                QString("ERROR: database :memory: at MEM failed to open - ")
                .arg(DiskDB.lastError().text());
        qDebug() << error_string;
        emit sendMessage(error_string);
    }
}

void SQtLiteData::openDiskDB(QString filename)
{
    DiskDB = QSqlDatabase::addDatabase("QSQLITE","DISK");
    DiskDB.setDatabaseName(filename);
    if (DiskDB.open()) {
        MemDB.close();
        createNewDB();
        QSqlQuery attach(MemDB);
        QString attachstr = QString("ATTACH DATABASE '%1' AS %2")
                .arg(filename)
                .arg(DiskDB.connectionName());
        if (attach.exec(attachstr)) {
            QSqlQuery insertquery(MemDB);
            QStringList tablesviews = DiskDB.tables(QSql::Tables);
            foreach (QString tableorview, tablesviews) {
                QString insertstr =
                        QString("INSERT OR REPLACE INTO '%1' SELECT * FROM %2.%1")
                        .arg(tableorview)
                        .arg(DiskDB.connectionName());
                if (!insertquery.exec(insertstr)){
                    qDebug() << "ERROR: openDiskDB INSERT failed - "
                             << insertquery.lastError();
                }
            }
            QSqlQuery detach(MemDB);
            QString detachstr = QString("DETACH %1").arg(DiskDB.connectionName());
            detach.exec(detachstr);
        } else {
            qDebug() << "ERROR: openDiskDB database ATTACH failed- "
                     << attach.lastError();
        }
        DiskDB.close();
    }  else {
        qDebug() << "ERROR: openDiskDB database filename at connection failed to open - "
                 << DiskDB.lastError();
    }
}

QSqlTableModel *SQtLiteData::getTableModel(QString table_name)
{
    QSqlTableModel *table_model = new QSqlTableModel(new QObject(),MemDB);
    table_model->setTable(table_name);
    return table_model;
}

void SQtLiteData::writeIDDSQL()
{
    QSqlQuery inserter(MemDB);
    for (int var = 0; var < Schema->children().count(); ++var) {
        SQtLiteObject *obj = qobject_cast<SQtLiteObject *>(Schema->children().at(var));
        QString sql = obj->getIDDSQLstring();
        if (!inserter.exec(sql))
            qDebug()<< sql << inserter.lastError();
        for (int var = 0; var < obj->children().count(); ++var) {
            SQtLiteObject *fld = qobject_cast<SQtLiteObject *>(obj->children().at(var));
            QString sql = fld->getIDDSQLstring();
            if (!inserter.exec(sql))
                qDebug()<< sql << inserter.lastError();
        }
    }
}

SQtLiteObject *SQtLiteData::getModel() const
{
    return Model;
}

SQtLiteObject *SQtLiteData::getSchema() const
{
    return Schema;
}

//FIXME: sql errors on db init
//"ERROR: query failed - near ")": syntax error Unable to execute statement"
//"ERROR: query failed - no such column: NEW.field_uuid Unable to execute statement"
//"ERROR: query failed - cannot commit - no transaction is active Unable to fetch row"
//FIXME: create DB from SQL, then pull in schema data,  --maintain or re-index tables one way or the other
