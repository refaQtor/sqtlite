#ifndef SQTLITEDATA_H
#define SQTLITEDATA_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlTableModel>

#include "sqtliteobject.h"

class SQtLiteData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString FileName READ FileName WRITE setFileName NOTIFY FileNameChanged)
    Q_PROPERTY(bool Saved READ Saved WRITE setSaved NOTIFY SavedChanged)


public:
    explicit SQtLiteData(QString filename = "", QObject *parent = 0);
    ~SQtLiteData();

    SQtLiteObject *getNewObject(QString object_type,
                                SQtLiteObject *parent,
                                SQtLiteObject::RootType root_type,
                                SQtLiteObject::DepthType depth_type);

    QString FileName() const;
    bool Saved() const;

    QStringList getTablenames();

    SQtLiteObject *getSchema() const;
    SQtLiteObject *getModel() const;

signals:
    void sendMessage(QString arg);
    void FileNameChanged(QString arg);
    void SavedChanged(bool arg);
    void dbLoaded();

public slots:
    void setFileName(QString arg);
    void setSaved(bool arg);

    void startup(QString filename);
    void createNewDB();
    void saveToDisk();
    void openDiskDB(QString filename);

    QSqlTableModel *getTableModel(QString table_name);

    void writeIDDSQL();

private:
    QSqlDatabase MemDB;
    QSqlDatabase DiskDB;

    SQtLiteObject *Schema;
    SQtLiteObject *Model;

    QString m_FileName;
    bool m_Saved;

};

#endif // SQTLITEDATA_H
