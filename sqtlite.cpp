#include "sqtlite.h"
#include "ui_sqtlite.h"

#include <QCommonStyle>
#include <QFileDialog>
#include <QStandardPaths>
#include <QActionGroup>

#include <QDebug>


SQtLite::SQtLite(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SQtLite),
    AppData(new SQtLiteData()),
    DisplayModel(new QSqlTableModel())
{
    ui->setupUi(this);
    setIcons();
    setDefaultPath();
    setupGroups();

    SchemaRoot = AppData->getSchema();
    connect(AppData,SIGNAL(sendMessage(QString)),
            this,SLOT(displayMessage(QString)));


    //TODO:these SIGNALS/SLOTS don't work?
    connect(AppData,SIGNAL(dbLoaded()),
            this,SLOT(updateDisplay()));
    connect(AppData,SIGNAL(FileNameChanged(QString)),
            this,SLOT(setWindowTitle(QString)));
    connect(AppData,SIGNAL(FileNameChanged(QString)),
            ui->statusBar,SLOT(showMessage(QString)));

    updateDisplay();
    ui->page_stack->setCurrentWidget(ui->about_page);
}

SQtLite::~SQtLite()
{
    delete ui;
}

void SQtLite::displayMessage(QString msg)
{
    ui->statusBar->showMessage(msg);
}

// actions
void SQtLite::on_actionNew_triggered()
{
    QString filename = QFileDialog::getSaveFileName
            (this,
             "Create New File",
             m_defaultpath,
             "SQtLite Files (*.sqt)");
    AppData = new SQtLiteData(filename);
    m_defaultpath = QDir(filename).absolutePath();
    updateDisplay();
}

void SQtLite::on_actionOpen_triggered()
{
    QString filename = QFileDialog::getOpenFileName
            (this,
             "Save as File Name",
             m_defaultpath,
             "SQtLite Files (*.sqt)");
    AppData->openDiskDB(filename);
    updateDisplay();
}

void SQtLite::on_actionSave_triggered()
{
    if (AppData->FileName() == "") //check just in case
        on_actionSave_As_triggered();
    else
        AppData->saveToDisk();
}

void SQtLite::on_actionSave_As_triggered()
{
    QString filename = QFileDialog::getSaveFileName
            (this,
             "Save as File Name",
             m_defaultpath,
             "SQtLite Files (*.sqt)");
    AppData->setFileName(filename);
    AppData->saveToDisk();
}

void SQtLite::on_actionQuit_triggered()
{
    close();
}

void SQtLite::on_actionIDFImport_triggered()
{
    ui->statusBar->showMessage("Not Imlemented.  Yet.");
}

void SQtLite::on_actionIDDImport_triggered()
{
    on_actionNew_triggered();
    QFile idd("://resources/Energy+v8-1-0-009-utf8.idd");
    if (idd.open(QFile::ReadOnly | QFile::Text))
    {
        loadSchemaObjectModel(idd);
        idd.close();
    } else {
        qDebug() << "ERROR: idd file failed to open : " << idd.errorString();
    }
}

void SQtLite::on_actionXMLImport_triggered()
{
    ui->statusBar->showMessage("Not Imlemented.  Yet.");
}

void SQtLite::on_actionJSONImport_triggered()
{
    ui->statusBar->showMessage("Not Imlemented.  Yet.");
}

void SQtLite::on_actionIDDxExport_triggered()
{
    ui->statusBar->showMessage("Not Imlemented.  Yet.");
}

void SQtLite::on_actionIDFxExport_triggered()
{
    ui->statusBar->showMessage("Not Imlemented.  Yet.");
}

void SQtLite::on_actionXMLExport_triggered()
{
    ui->statusBar->showMessage("Not Imlemented.  Yet.");
}

void SQtLite::on_actionJSONExport_triggered()
{
    ui->statusBar->showMessage("Not Imlemented.  Yet.");
}

void SQtLite::on_actionAbout_triggered()
{
    ui->page_stack->setCurrentWidget(ui->about_page);
    ui->actionAbout->setChecked(true);
}

void SQtLite::on_actionWork_triggered()
{
    ui->page_stack->setCurrentWidget(ui->work_page);
    ui->actionWork->setChecked(true);
}

void SQtLite::on_actionDisplay_triggered()
{
    ui->page_stack->setCurrentWidget(ui->display_page);
    ui->actionDisplay->setChecked(true);
}

void SQtLite::on_display_combo_currentIndexChanged(const QString &selected_table)
{
    QSqlTableModel *display_model(AppData->getTableModel(selected_table));
    display_model->setEditStrategy(QSqlTableModel::OnManualSubmit);
    display_model->select();
    ui->display_view->setModel(display_model);
    ui->display_view->show();
    this->displayMessage( display_model->lastError().text());
}

// setup
void SQtLite::setIcons()
{
    QCommonStyle *style = new QCommonStyle;
    ui->actionAbout->setIcon(style->standardIcon(QStyle::SP_MessageBoxQuestion));
    ui->actionDisplay->setIcon(style->standardIcon(QStyle::SP_FileDialogDetailedView));
    ui->actionWork->setIcon(style->standardIcon(QStyle::SP_DesktopIcon));
    ui->actionNew->setIcon(style->standardIcon(QStyle::SP_FileDialogNewFolder));
    ui->actionOpen->setIcon(style->standardIcon(QStyle::SP_DirOpenIcon));
    ui->actionSave->setIcon(style->standardIcon(QStyle::SP_DialogSaveButton));
}

void SQtLite::setDefaultPath()
{
    QStringList possibilities =
            QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
    m_defaultpath = possibilities.value(0,QDir::homePath());
}

void SQtLite::setupGroups()
{
    QActionGroup *viewGroup = new QActionGroup(this);
    viewGroup->addAction(ui->actionAbout);
    viewGroup->addAction(ui->actionDisplay);
    viewGroup->addAction(ui->actionWork);
    ui->actionAbout->setChecked(true);
}

void SQtLite::updateDisplay()
{
    loadDisplayCombo();
    on_actionDisplay_triggered();
}

void SQtLite::loadDisplayCombo()
{
    ui->display_combo->clear();
    QStringList tables = AppData->getTablenames();
    foreach (QString tablename, tables) {
        ui->display_combo->insertItem(0,tablename);
    }
}

QString SQtLite::cropLineType(QString line, QString type)
{
    QString pattern = QString("\\\\%1 ").arg(type);
    return line.remove(QRegExp(pattern)).trimmed();
}

QStringList SQtLite::extractCleanStringListFromFile(QFile &idd)
{
    QString all(idd.readAll());
    QStringList iddstrings(all.split("\n"));

    QStringList not_remarks;
    QStringList keepers;


    foreach (QString raw_line, iddstrings) {
        QStringList oneline = raw_line.split("!"); //find lines with comments
        not_remarks.append(oneline.value(0).trimmed()); //discard part after !, if any
    }

    int position = 0;
    foreach (QString keep, not_remarks) {
        if (keep.startsWith("\\")){ //most common case
            keepers.append(keep.trimmed());
        }
        else if (keep.contains(QRegExp("[A-Z]+[0-9]+[ ]*[,|;][ ]*\\\\field "))){ //field, second most common
            //            field_cnt++;
            position++;
            //tracking Field position per Object and prepending it on this line for later extraction
            QString new_fieldname = QString("%1~%2").arg(QString::number(position)).arg(keep.trimmed());
            keepers.append(new_fieldname);
        }
        else if (keep.contains(QRegExp("[A-Z]+[a-z]+([:]{1}|[A-Z]+[a-z]*)*[,|;]"))){ //object, prepend \object to normalize processing
            //            object_cnt++;
            position = 0; //reset for new Object
            QString object_string =  QString("\\object %1").arg(keep.trimmed());
            keepers.append(object_string.remove(QRegExp("[,|;]$")));
            //            qDebug() << object_string;
        }
    }
    return keepers;
}

void SQtLite::loadSchemaObjectModel(QFile &idd)
{
    QString active_group = "";
    SQtLiteObject *active_qobject;
    SQtLiteObject *active_qfield;

    int object_cnt = 0;
    int group_cnt = 0;
    int field_cnt = 0;
    int min_fields = 100; //keeps it running while get this set after 'extens' is set
    bool extens = false;
    QString obj_name("");

    QStringList keepers = extractCleanStringListFromFile(idd);

    foreach (QString current_line, keepers) {
        if (current_line.contains("\\group ")){ //GROUP LEVEL
            group_cnt++;
            active_group = cropLineType(current_line,"group");
        } else if (current_line.startsWith("\\object ")){ //OBJECT LEVEL
            object_cnt++;
            min_fields = 100;
            field_cnt = 0;
            extens = false;
            QString original_name = cropLineType(current_line,"object");
            active_qobject = AppData->getNewObject(cropLineType(current_line,"object"),
                                                   AppData->getSchema(),
                                                   SQtLiteObject::Root_Schema,
                                                   SQtLiteObject::Depth_Object);
            active_qobject->setProperty("original_name",original_name);
            active_qobject->setProperty("object_group", active_group);
//            obj_name = cropLineType(current_line,"object");
//            active_qobject->setObjectName(obj_name);
            active_qobject->setProperty("depth","object");
            active_qobject->setProperty("uuid",QUuid::createUuid().toString());


        }

        else if (current_line.startsWith("\\memo ")){
            QString current_memo = active_qobject->property("memo").toString();
            QString new_memo = QString("%1 %2")
                    .arg(current_memo)
                    .arg(cropLineType(current_line,"memo"));
            active_qobject->setProperty("memo",current_memo);
        } else if (current_line.startsWith("\\unique-object")){
            active_qobject->setProperty("unique_object","True");
        } else if (current_line.startsWith("\\required-object")){
            active_qobject->setProperty("required_object","True");
        } else if (current_line.startsWith("\\min-fields ")){
            int min_fields = cropLineType(current_line,"min-fields").toInt();
            active_qobject->setProperty("minimum_fields",min_fields);
        } else if (current_line.startsWith("\\obsolete")){
            active_qobject->setProperty("obsolete",cropLineType(current_line,"obsolete"));
        } else if (current_line.startsWith("\\extensible:")){
            extens = true;
            QString pattern = QString("\\\\extensible:");
            current_line.remove(QRegExp(pattern)).trimmed();
            QString propstr = current_line.split(" ").at(0);
            active_qobject->setProperty("extensible", propstr);

        } else if (current_line.startsWith("\\format ")){
            active_qobject->setProperty("format",cropLineType(current_line,"format"));
        } // else if (good.startsWith("\\reference-class-name ")){} //never encountered

        if ((!active_qobject->property("extensible").toBool())||((active_qobject->property("extensible").toBool()) && (field_cnt < active_qobject->property("minimum_fields").toInt()))){
            if (current_line.contains("\\field ")){ //FIELD LEVEL
                field_cnt++;

                QString line = cropLineType(current_line, "field");
                QString holder = line;
                QRegExp pos("~.*");
                QString pstn = holder.remove(pos);
                holder = line; //again
                QRegExp regx_front("[0-9]*~");
                QRegExp regx_back("[ ]*[,|;].*");
                QString spec = holder.remove(regx_front) //remove front
                        .remove(regx_back); //remove back
                QString field_name = line.remove(regx_front)
                        .remove(QRegExp("[A-Z]+[0-9]+[ ]*[,|;][ ]*"));
                active_qfield = AppData->getNewObject(field_name, active_qobject, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);;
                active_qfield->setProperty("original_name",field_name);
                active_qfield->setProperty("depth","field");
                //next, name repeated here to normalize processing
                active_qfield->setProperty("specifier", spec);
                QString  spec_type = spec.at(0);
                active_qfield->setProperty("data_type",spec_type);
                active_qfield->setProperty("position",pstn);
                //next, initialize data for all other fields for normalized processing


            } else if (current_line.startsWith("\\note ")){
                QString current_note = active_qobject->property("note").toString();
                QString new_note = QString("%1 %2")
                        .arg(current_note)
                        .arg(cropLineType(current_line,"note"));
                active_qfield->setProperty("note", new_note);
            } else if (current_line.startsWith("\\required-field")){
                active_qfield->setProperty("required_field", "True");
            } else if (current_line.startsWith("\\begin-extensible")){
                active_qfield->setProperty("begin_extensible", true);
            } else if (current_line.startsWith("\\units ")){
                active_qfield->setProperty("units", cropLineType(current_line,"units"));
            } else if (current_line.startsWith("\\ip-units ")){
                active_qfield->setProperty("ip_units", cropLineType(current_line,"ip-units"));
            } else if (current_line.startsWith("\\unitsBasedOnField ")){
                active_qfield->setProperty("units_based_on_field", cropLineType(current_line,"unitsBasedOnField"));
            } else if (current_line.startsWith("\\minimum ")){
                active_qfield->setProperty("minimum", cropLineType(current_line,"minimum"));
            } else if (current_line.startsWith("\\minimum> ")){
                active_qfield->setProperty("include_minimum", cropLineType(current_line,"minimum>"));
            } else if (current_line.startsWith("\\maximum ")){
                active_qfield->setProperty("maximum", cropLineType(current_line,"maximum"));
            } else if (current_line.startsWith("\\maximum< ")){
                active_qfield->setProperty("include_maximum", cropLineType(current_line,"maximum<"));
            } else if (current_line.startsWith("\\default ")){
                active_qfield->setProperty("value_default", cropLineType(current_line,"default"));
            } else if (current_line.startsWith("\\deprecated")){
                active_qfield->setProperty("deprecated", cropLineType(current_line,"deprecated"));
            } else if (current_line.startsWith("\\autosizable")){
                active_qfield->setProperty("autosizable", cropLineType(current_line,"autosizable"));
            } else if (current_line.startsWith("\\autocalculatable")){
                active_qfield->setProperty("autocalculatable", cropLineType(current_line,"autocalculatable"));
            } else if (current_line.startsWith("\\retaincase")){
                active_qfield->setProperty("retaincase", cropLineType(current_line,"retaincase"));
            } else if (current_line.startsWith("\\key ")){ //append each new 'key'
                QString current_choices = active_qobject->property("key").toString();
                QString new_choices = QString("%1|%2")
                        .arg(current_choices)
                        .arg(cropLineType(current_line,"key"));
                active_qfield->setProperty("choices", new_choices);
            } else if (current_line.startsWith("\\object-list ")){
                active_qfield->setProperty("object_list", cropLineType(current_line,"object-list"));
            } else if (current_line.startsWith("\\external-list ")){
                active_qfield->setProperty("external_list", cropLineType(current_line,"external-list"));
            } else if (current_line.startsWith("\\reference ")){
                active_qfield->setProperty("reference", cropLineType(current_line,"reference"));
            }
        }
    }

//    writeQObjectModelasXML(root_schema_object, "idd_schema.xml");
    qDebug("done processing IDD");
//    QString child("Building");
//    SQtLiteObject *notroot = qobject_cast<SQtLiteObject * >(SchemaRoot);
// notroot->recordIDDProperties();

 AppData->writeIDDSQL();
    AppData->saveToDisk();
updateDisplay();
}


void SQtLite::on_pushButton_clicked()
{
    SQtLiteObject *test_object = AppData->getNewObject("Building", SchemaRoot, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Object);
    test_object->setProperty("one","1");
    SQtLiteObject *test_field = AppData->getNewObject("Area", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *q = AppData->getNewObject("sdf", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *w = AppData->getNewObject("gd", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *e = AppData->getNewObject("Arehfhga", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *r = AppData->getNewObject("dfjg", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *t = AppData->getNewObject("jfgj", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *y = AppData->getNewObject("fukf", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *u = AppData->getNewObject("gjdfgn", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *i = AppData->getNewObject("gjdg", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *o = AppData->getNewObject("jgddddddd", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *p = AppData->getNewObject("jhj", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    SQtLiteObject *a = AppData->getNewObject("hjjdghj", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    ui->pushButton->setText(test_object->objectName() + test_field->objectName());
    updateDisplay();
}


void SQtLite::on_pushButton_2_clicked()
{
    SQtLiteObject *test_object = AppData->getNewObject("House", SchemaRoot, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Object);
    SQtLiteObject *test_field = AppData->getNewObject("Garage", test_object, SQtLiteObject::Root_Schema, SQtLiteObject::Depth_Field);
    ui->pushButton->setText(test_object->objectName() + test_field->objectName());
    updateDisplay();
}



//FIXME: note not extracted correctly
//FIXME: memo not extracted correctly
//FIXME: retaincase not extracted correctly
//FIXME: choices have pipe on front
