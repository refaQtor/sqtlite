An example of an application using a consolidated application file format expressing a combination of hierarchical and flat data models that can be created, accessed, and manipulated identically in memory or on disk. When saved to disk, it contains all the artifacts for complex applications in a single file that can be passed around and stored like any other file.

This SQtLite example application relies on some strengths Qt, but the data is saved to disk as a single SQLite file.

SQtLite relies on some strengths of SQLite and Qt.

http://sqlite.org/appfileformat.html

http://qt-project.org/doc/qt-5/properties.html

http://qt-project.org/doc/qt-5/object.html

The original application that created the file needn't be used for viewing and accessing your data in the file.  It can be easily read, manipulated, exported AND extended by any other application that works with SQLite, such as:

SQLiteManager : (Firefox addon) https://code.google.com/p/sqlite-manager/

SQLiteBrowser : (win/mac/lin) http://sqlitebrowser.org/

SQLiteMan : (win/lin) http://sourceforge.net/projects/sqliteman/?source=directory

SQLiteStudio : (win/mac/lin) http://sqlitestudio.pl/?act=about

The schema is encoded in the file with well documented SQL that other developers can access in a standard way to integrate this file into their applications as a producer or consumer. And SQL is a great way to store and access huge amounts of numerical data.