CREATE TABLE schema_objects (
uuid TEXT PRIMARY KEY,
object_type TEXT UNIQUE NOT NULL,
depth TEXT,
extensible INTEGER,
object_group TEXT,
memo TEXT,
minimum_fields INTEGER,
original_name TEXT,
required_object INTEGER,
unique_object INTEGER,
obsolete INTEGER,
format TEXT,
version REAL);

CREATE TABLE schema_fields (
uuid TEXT PRIMARY KEY,
object_uuid TEXT NOT NULL,
field_type TEXT NOT NULL,
depth TEXT,
position INTEGER,
data_type TEXT,
specifier TEXT,
autocalculatable INTEGER,
autosizable INTEGER,
begin_extensible INTEGER,
choices TEXT,
default_value REAL,
include_maximum INTEGER,
include_minimum INTEGER,
maximum_value TEXT,
minimum_value TEXT,
original_name TEXT,
note TEXT,
object_list TEXT,
reference TEXT,
required_field INTEGER,
retaincase INTEGER,
object_type TEXT,
units TEXT,
ip_units TEXT,
deprecated INTEGER,
units_based_on_field TEXT,
external_list TEXT,
FOREIGN KEY (object_uuid)
REFERENCES schema_objects(uuid));

CREATE TABLE models (
uuid TEXT PRIMARY KEY,
model_label TEXT,
note TEXT,
version REAL);

CREATE TABLE objects (
uuid TEXT PRIMARY KEY,
models_uuid TEXT,
object_label TEXT,
object_group TEXT,
FOREIGN KEY (models_uuid)
REFERENCES models(uuid));

CREATE TABLE fields (
uuid TEXT PRIMARY KEY,
objects_uuid TEXT,
field_label TEXT,
field_value REAL,
FOREIGN KEY (objects_uuid)
REFERENCES objects(uuid));


CREATE VIEW group_types
AS SELECT DISTINCT object_group
FROM schema_objects;

CREATE VIEW group_count
AS SELECT count(*)
FROM group_types;

CREATE VIEW object_types
AS SELECT DISTINCT object_type
FROM schema_objects;

CREATE VIEW objects_count
AS SELECT count(*)
FROM object_types;

CREATE VIEW schema_view
AS SELECT o.object_group AS 'Group',
o.object_type AS 'Object',
f.field_type AS 'Field',
f.data_type AS 'Type'
FROM schema_objects o,
schema_fields f
WHERE f.object_uuid = o.uuid;

CREATE VIEW model_view
AS SELECT m.uuid AS 'Model',
o.object_group AS 'Group',
o.object_type AS 'Object',
f.field_type AS 'Field',
d.field_value AS 'Value',
f.data_type AS 'Type'
FROM schema_objects o,
schema_fields f,
fields d,
models m
WHERE f.object_uuid = o.uuid
AND d.uuid = f.uuid;

CREATE VIEW NAME_CHANGES
AS SELECT object_type,
original_name
FROM schema_objects;

CREATE VIEW SCHEMA_DATA
AS SELECT o.uuid as object_uuid,
o.object_type,
o.minimum_fields,
o.original_name,
o.required_object,
o.unique_object,
f.uuid as field_uuid,
f.field_type,
f.position,
f.data_type,
f.specifier,
f.begin_extensible,
f.choices,
f.default_value,
f.include_maximum,
f.include_minimum,
f.maximum_value,
f.minimum_value,
f.required,
f.units
FROM schema_objects o,
schema_fields f
WHERE f.object_uuid = o.uuid;

CREATE TRIGGER schema_insert_replace
INSTEAD OF INSERT ON SCHEMA_DATA
FOR EACH ROW
BEGIN
INSERT OR REPLACE INTO schema_objects (uuid, object_type)
VALUES (NEW.object_uuid, NEW.object_type);
INSERT INTO schema_fields (uuid, object_uuid, field_type, units)
VALUES (NEW.field_uuid, NEW.object_uuid, NEW.field_type, NEW.units);
END;


CREATE TABLE weather_location (
uuid TEXT PRIMARY KEY,
location TEXT,
city TEXT,
territory TEXT,
country TEXT,
latitude REAL,
longitude REAL,
timezone REAL);

CREATE TABLE weather_data (
location_uuid TEXT,
epoch_ts INTEGER,
dry_bulb REAL,
pressure REAL,
insolation REAL,
FOREIGN KEY (location_uuid)
REFERENCES weather_location(uuid));

CREATE VIEW weather
AS SELECT l.location AS 'Location',
l.city AS 'City',
l.territory AS 'State',
l.country AS 'Country',
l.latitude AS 'Latitude',
l.longitude AS 'Longitude',
d.dry_bulb AS 'Dry Bulb'
FROM weather_location l,
weather_data d
WHERE d.location_uuid = l.uuid;

CREATE VIEW temperature
AS SELECT l.location AS 'Location',
l.city AS 'City',
d.epoch_ts AS 'Time',
d.dry_bulb AS 'Temperature'
FROM weather_location l,
weather_data d
WHERE d.location_uuid = l.uuid;
