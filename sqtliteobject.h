/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef SQTLITEOBJECT_H
#define SQTLITEOBJECT_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QUuid>
#include <QXmlStreamWriter>



const QString ENERGYPLUSXMLNAMESPACE="http://energyplus.doe.gov/2014/XMLSchema";
const QString XSDNAMESPACE="http://www.w3.org/2001/XMLSchema";

class SQtLiteObject : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString ObjectType READ ObjectType CONSTANT)
    Q_PROPERTY(QUuid uuid READ uuid CONSTANT)
    Q_PROPERTY(RootType Root_Type READ Root_Type CONSTANT)
public:
    enum RootType {
        Root_Schema = 0,
        Root_Model
    };
    enum DepthType {
        Depth_Root = 0,
        Depth_Object,
        Depth_Field
    };

    SQtLiteObject(QString object_type,
                  SQtLiteObject *parent,
                  SQtLiteObject::RootType root_type,
                  SQtLiteObject::DepthType depth_type,
                  QSqlTableModel *object_table_model,
                  QSqlTableModel *field_table_model);

    QString ObjectType() const;
    QUuid uuid() const;

    RootType Root_Type() const;

    void recordIDDProperties();
    SQtLiteObject * parentPointer();
public slots:

    void setObjectName(QString new_name);
    QString objectName();
    void setProperty(const char *name, const QVariant &value);

    QString getIDDSQLstring();

    void toXMLdata(QXmlStreamWriter &xml);
    void toXMLattributes(QXmlStreamWriter &xml); //just for full example for editix xsd gen

private:


    //only internal types, dynamic types added after instantiation
    QUuid m_Uuid;
    QString m_ObjectType;
    RootType m_Root_Type;
    DepthType m_Depth_Type;
    QSqlTableModel *m_ObjectTable;
    QSqlTableModel *m_FieldTable;

    //utility
    QString cleanXML(QString dirty);  //TODO: put this in the right spot
    QString cleanSQL(QString dirty);


    Q_DISABLE_COPY(SQtLiteObject)

};


#endif // SQTLITEOBJECT_H
