#ifndef SQTLITE_H
#define SQTLITE_H

#include <QFile>
#include <QMainWindow>
#include <QSqlTableModel>

#include "sqtlitedata.h"

namespace Ui {
class SQtLite;
}

class SQtLite : public QMainWindow
{
    Q_OBJECT

public:
    explicit SQtLite(QWidget *parent = 0);
    ~SQtLite();

    void setupGroups();
public slots:
    void displayMessage(QString msg);
    void updateDisplay();

private slots:
    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void on_actionSave_As_triggered();

    void on_actionQuit_triggered();

    void on_actionIDDImport_triggered();

    void on_actionIDFImport_triggered();

    void on_actionXMLImport_triggered();

    void on_actionJSONImport_triggered();

    void on_actionIDDxExport_triggered();

    void on_actionIDFxExport_triggered();

    void on_actionXMLExport_triggered();

    void on_actionJSONExport_triggered();

    void on_actionAbout_triggered();

    void on_actionWork_triggered();

    void on_actionDisplay_triggered();

    void on_display_combo_currentIndexChanged(const QString &arg1);

    void on_pushButton_clicked();





    void on_pushButton_2_clicked();

private:
    Ui::SQtLite *ui;
    SQtLiteData *AppData;
    SQtLiteObject *SchemaRoot;
    SQtLiteObject *ModelRoot;

    QSqlTableModel *DisplayModel;

    QString m_defaultpath;

    void setIcons();
    void setDefaultPath();


    void loadDisplayCombo();
    QString cropLineType(QString line, QString type);
    QStringList extractCleanStringListFromFile(QFile &idd);
    void loadSchemaObjectModel(QFile &idd);
};

#endif // SQTLITE_H
