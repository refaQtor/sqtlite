#-------------------------------------------------
#
# Project created by QtCreator 2014-07-19T17:50:40
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SQtLite
TEMPLATE = app


SOURCES += main.cpp\
        sqtlite.cpp \
    sqtlitedata.cpp \
    sqtliteobject.cpp

HEADERS  += sqtlite.h \
    sqtlitedata.h \
    sqtliteobject.h

FORMS    += sqtlite.ui

OTHER_FILES += \
    resources/EnergyPlus.sql

RESOURCES += \
    resources.qrc
