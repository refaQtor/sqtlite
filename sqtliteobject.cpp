/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "sqtliteobject.h"

#include <QDebug>
#include <QRegExp>
#include <QSqlRecord>
#include <QSqlQuery>
#include <QSqlError>
#include <QXmlStreamWriter>




SQtLiteObject::SQtLiteObject(QString object_type,
                             SQtLiteObject *parent,
                             RootType root_type,
                             DepthType depth_type,
                             QSqlTableModel *object_table_model,
                             QSqlTableModel *field_table_model):
    QObject(parent),
    m_Uuid(QUuid::createUuid()),
    m_ObjectType(cleanXML(object_type)),
    m_Root_Type(root_type),
    m_Depth_Type(depth_type),
    m_ObjectTable(object_table_model),
    m_FieldTable(field_table_model)
{

}

QString SQtLiteObject::ObjectType() const
{
    return m_ObjectType;
}

QUuid SQtLiteObject::uuid() const
{
    return m_Uuid;
}

SQtLiteObject::RootType SQtLiteObject::Root_Type() const
{
    return m_Root_Type;
}


void SQtLiteObject::setObjectName(QString new_name)
{
 //   override with a no-op, the the name must be clean for other algorithms
    ;
}

QString SQtLiteObject::objectName()
{
    return m_ObjectType;
}

void SQtLiteObject::setProperty(const char *name, const QVariant &value)
{
    QObject::setProperty(name,value);
    //    if () obj/fld...

}

SQtLiteObject * SQtLiteObject::parentPointer()
{
    SQtLiteObject *prnt = qobject_cast<SQtLiteObject *>(parent());

    return prnt;
}

QString SQtLiteObject::getIDDSQLstring()
{
    QString sqlstring;

    if ("root" == property("depth")){
        qDebug("root");
    } else if ("object" == property("depth")) {
        sqlstring.append(QString("INSERT INTO schema_objects ( depth,  uuid, object_type, object_group, memo, unique_object, required_object, minimum_fields, obsolete, extensible, format, original_name, version )\
                              VALUES ( '%1', '%2', '%3', '%4', '%5', '%6', '%7', '%8', '%9', '%10', '%11', '%12', '%13'); ")
                .arg(cleanSQL(property("depth").toString()))
                .arg(m_Uuid.toString())
                .arg(m_ObjectType)
                .arg(cleanSQL(property("object_group").toString()))
                .arg(cleanSQL(property("memo").toString()))
                .arg(cleanSQL(property("unique_object").toString()))
                .arg(cleanSQL(property("required_object").toString()))
                .arg(cleanSQL(property("minimum_fields").toString()))
                .arg(cleanSQL(property("obsolete").toString()))
                .arg(cleanSQL(property("extensible").toString()))
                .arg(cleanSQL(property("format").toString()))
                .arg(cleanSQL(property("original_name").toString()))
                         .arg("1"));


    } else if ("field" == property("depth")) {
        SQtLiteObject *prnt = parentPointer();
        QString prnt_uuid = prnt->uuid().toString();
        sqlstring.append(QString("INSERT INTO schema_fields ( uuid, field_type, object_uuid, depth, specifier, data_type, position, note, required_field, begin_extensible, units, ip_units, units_based_on_field, minimum_value, include_minimum, maximum_value, include_maximum, default_value, deprecated, autosizable, autocalculatable, retaincase, choices, object_list, external_list, reference, original_name ) \
                            VALUES ( '%1', '%2', '%3', '%4', '%5', '%6', '%7', '%8', '%9', '%10', '%11', '%12','%13', '%14', '%15', '%16', '%17', '%18', '%19', '%20', '%21', '%22', '%23', '%24', '%25', '%26', '%27'); ")
                .arg(m_Uuid.toString())
                .arg(m_ObjectType)
                .arg(prnt_uuid)
                .arg(cleanSQL(property("depth").toString()))
                .arg(cleanSQL(property("specifier").toString()))
                .arg(cleanSQL(property("data_type").toString()))
                .arg(cleanSQL(property("position").toString()))
                .arg(cleanSQL(property("note").toString()))
                .arg(cleanSQL(property("required_field").toString()))
                .arg(cleanSQL(property("begin_extensible").toString()))
                .arg(cleanSQL(property("units").toString()))
                .arg(cleanSQL(property("ip_units").toString()))
                .arg(cleanSQL(property("units_based_on_field").toString()))
                .arg(cleanSQL(property("minimum").toString()))
                .arg(cleanSQL(property("include_minimum").toString()))
                .arg(cleanSQL(property("maximum").toString()))
                .arg(cleanSQL(property("include_maximum").toString()))
                .arg(cleanSQL(property("value_default").toString()))
                .arg(cleanSQL(property("deprecated").toString()))
                .arg(cleanSQL(property("autosizable").toString()))
                .arg(cleanSQL(property("autocalculatable").toString()))
                .arg(cleanSQL(property("retaincase").toString()))
                .arg(cleanSQL(property("choices").toString()))
                .arg(cleanSQL(property("object_list").toString()))
                .arg(cleanSQL(property("external_list").toString()))
                .arg(cleanSQL(property("reference").toString()))
                .arg(cleanSQL(property("original_name").toString())) );
    }

    return sqlstring;

    //    QString sql = QString("INSERT INTO schema_objects ( depth,  uuid, object_type, group, memo, unique_object, required_object, min_fields, obsolete, extensible, format, original_name ) VALUES ( %1, %2, %3, %4, %5, %6, %7, %8, %9, %10, %11, %12); ")
    //            .arg(property("depth"))
    //            .arg(m_Uuid.toString())
    //            .arg(m_ObjectType)
    //            .arg(property("group"))
    //            .arg(property("memo"))
    //            .arg(property("unique_object"))
    //            .arg(property("required_object"))
    //            .arg(property("minimum_fields"))
    //            .arg(property("obsolete"))
    //            .arg(property("extensible"))
    //            .arg(property("format"))
    //            .arg(property("original_name")) ;

}




void SQtLiteObject::toXMLdata(QXmlStreamWriter &xml)
{
    //just temporarily call other method
    //    this->toXMLattributes(xml);


    xml.writeStartElement(ENERGYPLUSXMLNAMESPACE, this->objectName());

    //    QString depth = property("depth").toString();
    //    if (!(depth == "root")) {
    QString idstr = QString("%1").arg(this->property("uuid").toString());
    if (!idstr.isEmpty()) //objects have uuid
        xml.writeAttribute("uuid", idstr);

    QString valstr = QString("%1").arg(this->property("value").toString());
    if (idstr.isEmpty()) //fields have no uuid
        if (!valstr.isEmpty())
            xml.writeAttribute("value", valstr);

    for (int var = 0; var < this->children().count(); ++var) {
        qobject_cast<SQtLiteObject *>(this->children().at(var))->toXMLdata(xml);
    }

    xml.writeEndElement();

    //    }
}


void SQtLiteObject::toXMLattributes(QXmlStreamWriter &xml)
{
    xml.writeStartElement(ENERGYPLUSXMLNAMESPACE, this->objectName());

    QString idstr = QString("%1").arg(this->property("uuid").toString());
    if (!idstr.isEmpty()) //objects have uuid
        xml.writeAttribute("uuid", idstr);
    QList<QByteArray> dynprops = this->dynamicPropertyNames();
    for (int var = 0; var < dynprops.count(); ++var) {

        QString attrstr = dynprops.value(var);
        //            if ((attrstr == "memo") || (attrstr == "note"))
        //                break;
        QString valstr = this->property(attrstr.toLocal8Bit()).toString();
        //            if (!valstr.isEmpty()) {
        xml.writeAttribute(attrstr, valstr);
        //            }
    }

    //    QString valstr = QString("%1").arg(this->property("value").toString());
    if (idstr.isEmpty()) //fields have no uuid
    {
        QList<QByteArray> dynprops = this->dynamicPropertyNames();
        for (int var = 0; var < dynprops.count(); ++var) {

            QString attrstr = dynprops.value(var);
            //            if ((attrstr == "memo") || (attrstr == "note"))
            //                break;
            QString valstr = this->property(attrstr.toLocal8Bit()).toString();
            //            if (!valstr.isEmpty()) {
            xml.writeAttribute(attrstr, valstr);
            //            }
        }
        xml.writeAttribute("value","some value");
    }
    for (int var = 0; var < this->children().count(); ++var) {
        qobject_cast<SQtLiteObject *>(this->children().at(var))->toXMLdata(xml);
    }

    xml.writeEndElement();
}

//TODO: there seems some redundancy/overlap of the items passed to the constructor
//fix it

//TODO: make this really record the dynamic properties - this is just to capture IDD
void SQtLiteObject::recordIDDProperties()
{
    m_ObjectTable->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_ObjectTable->select();
    m_FieldTable->setEditStrategy(QSqlTableModel::OnManualSubmit);
    m_FieldTable->select();

    QSqlRecord o_record( m_ObjectTable->record());
    QSqlRecord f_record( m_FieldTable->record());

    if ("root" == property("depth")){
        qDebug("root");
    } else if ("object" == property("depth")) {
        o_record.setValue(o_record.indexOf("depth"), property("depth").toString());
        o_record.setValue("uuid", m_Uuid);
        o_record.setValue("object_type", m_ObjectType);
        o_record.setValue("object_group", property("object_group").toString());
        o_record.setValue("memo", property("memo").toString());
        o_record.setValue("unique_object", property("unique_object").toString());
        o_record.setValue("required_object", property("required_object").toString());
        o_record.setValue("min_fields", property("minimum_fields").toString());
        o_record.setValue("obsolete", property("obsolete").toString());
        o_record.setValue("extensible", property("extensible").toString());
        o_record.setValue("format", property("format").toString());
        o_record.setValue("original_name", property("original_name").toString());

        if (!m_ObjectTable->insertRecord(-1,o_record)){
            qDebug() << m_ObjectTable->lastError();
            qDebug() << m_ObjectTable->query().lastQuery();
        }
        if (!m_ObjectTable->submitAll()){
            qDebug() << m_ObjectTable->lastError();
            qDebug() << m_ObjectTable->query().lastQuery();
        }
        //        qDebug() << "object created : " << objectName() ;//nothing yet
    } else if ("field" == property("depth")) {
        f_record.setValue("uuid", m_Uuid);
        f_record.setValue("field_type", m_ObjectType);
        f_record.setValue("object_uuid", property("object_uuid").toString());
        f_record.setValue("depth", property("depth").toString());
        f_record.setValue("specifier", property("specifier").toString());
        f_record.setValue("data_type", property("data_type").toString());
        f_record.setValue("position", property("position").toString());
        f_record.setValue("note", property("note").toString());
        f_record.setValue("required_field", property("required_field").toString());
        f_record.setValue("begin_extensible", property("begin_extensible").toString());
        f_record.setValue("units", property("units").toString());
        f_record.setValue("ip_units", property("ip_units").toString());
        f_record.setValue("units_based_on_field", property("units_based_on_field").toString());
        f_record.setValue("minimum", property("minimum").toString());
        f_record.setValue("include_minimum", property("include_minimum").toString());
        f_record.setValue("maximum", property("maximum").toString());
        f_record.setValue("include_maximum", property("include_maximum").toString());
        f_record.setValue("default_value", property("value_default").toString());
        f_record.setValue("deprecated", property("deprecated").toString());
        f_record.setValue("autosizable", property("autosizable").toString());
        f_record.setValue("autocalculatable", property("autocalculatable").toString());
        f_record.setValue("retaincase", property("retaincase").toString());
        f_record.setValue("choices", property("choices").toString());
        f_record.setValue("object_list", property("object_list").toString());
        f_record.setValue("external_list", property("external_list").toString());
        f_record.setValue("reference", property("reference").toString());
        f_record.setValue("original_name", property("original_name").toString());

        if (!m_FieldTable->insertRecord(-1,f_record)){
            qDebug() << m_FieldTable->lastError();
        }
        if (!m_FieldTable->submitAll()){
            qDebug() << m_FieldTable->lastError();
            qDebug() << m_FieldTable->query().lastQuery();
        }
        //        qDebug() << "field created : " << objectName() ;//nothing yet
    } else {
        qDebug("wonk!");
    }

    for (int var = 0; var < children().count(); ++var) {
        recordIDDProperties();
    }
}

QString SQtLiteObject::cleanXML(QString dirty)
{ //heavy handed approach to remove any problem characters for xml
    QString clean = dirty.replace(" ","_")
            .replace(":","_")
            .replace("?","_")
            .replace("/","_")
            .replace(",","_")
            .replace("\\","_")
            .replace("(","_")
            .replace(")","_")
            .replace("'","_")
            .replace("[","_")
            .replace("]","_")
            .replace("*","_")
            .replace("+","_")
            .replace("-","_")
            .replace("=","_")
            .replace("100%","Percent")
            .replace("%","_")
            .replace("#","_")
            .replace("{","_")
            .replace("}","_")
            .replace("$","_");
    return clean;
}

QString SQtLiteObject::cleanSQL(QString dirty)
{//heavy handed approach to remove any problem characters for sql
    QString clean = dirty.replace(","," ")
            .replace("'","");
    return clean;
}

